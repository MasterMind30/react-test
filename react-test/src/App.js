import React, { Component } from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Visitor from './Visitor';
import AddVisitor from './AddVisitor'

/*import AddVisitor from './AddVisitor';
import Visitor from './Visitor';*/

/*const visitor = [
{
  name:'Daniel',
  surname:"Portillo",
  countries:"Venezuela",
  birthday:"1991-03-16"
}
];s

localStorage.setItem('visitor',JSON.stringify(visitor));*/



class App extends Component {
constructor(props){
  super(props);

  this.state = {
    visitor: JSON.parse(localStorage.getItem('visitor')) || []
   
  }
  this.onAdd = this.onAdd.bind(this);
 
}



componentWillMount(){
const visitor = this.getVisitor();

this.setState({visitor});
document.body.style.backgroundColor = "#EEEEEE"
}

getVisitor(){
  return this.state.visitor;

}

onAdd(name , surname , countries , birthday){
  const visitor = this.getVisitor();
  visitor.push({
    name,
    surname,
    countries,
    birthday
  });
  let date = birthday.split('-');
  var year = date[0];
  var month = date[1];
  var day = date[2];
  localStorage.setItem('visitor',JSON.stringify(visitor)) ;
  this.setState({visitor});
  this.setState({message:"Hello "+name+surname+ " from " + countries + " . on " +day+ " of "+ month+ " you will have "  +year});
  
  this.setState({newYear:day + "-" +month +"-"+ year});
  const message = this.state.message;
}


 
  render() {
    return (
<body style={{backgroundColor: "#EEEEEE"}}>
<div className="container">
  <div className="col-xs-12 col-sm-12" style={{color:"#4A88CE" , "fontWeight": "bold" , "textAlign" : "center" , "fontSize": "30px"}}>Intive - FDV Exercise</div>
  <div className="row">
    <div className="col-xs-6 col-sm-6">
      <AddVisitor 
       onAdd = {this.onAdd}
       />
    <span className="alert-success"> {this.state.message}</span>
    </div>
  <div className="col-sm-6">
    <table className="table" style={{border: "solid","borderColor": "#4A88CE" }}>
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Country</th>
          <th scope="col">birthday</th>
          
        </tr>
      </thead>
   
       {
        this.state.visitor.map(visitor => {
          return(
           
            <Visitor
            key = {visitor.name}
            {...visitor}
          
            />

            );
        })

       }
      
    </table>
    <div className="col-sm-12" style={{color:"#4A88CE" , "fontWeight": "bold"  , "textAlign":"right" , "fontSize":"20px"}}>Daniel Portillo</div>
    </div>
  </div>
</div>
 
</body>
    );
  }
}

export default App;
