import React, { Component } from 'react';



class AddVisitor extends Component {
constructor(props){
  super(props);
  this.onSubmit = this.onSubmit.bind(this);
  this.state = {
    comboBox:[]
  }
}


componentWillMount(){
  fetch('https://restcountries.eu/rest/v2/all')
    .then(response => response.json()) 
    .then((response) => {
      const result = response; 
      
     this.setState({comboBox:result});
      console.log(this.state.comboBox);
    })
}


onSubmit(event){
  event.preventDefault();
  this.props.onAdd(this.nameInput.value , this.surnameInput.value,this.countriesInput.value,this.birthdayInput.value);
  
  this.nameInput.value = '';
  this.surnameInput.value = '';
  this.countriesInput.value = '';
  this.birthdayInput.value = '';
}
  render() {
      
    return (
      <form onSubmit={this.onSubmit}>
      <div className="row">
      <span style={{color:"#4A88CE" , "fontWeight": "bold"}}>Name : &nbsp;</span>
      <input required style={{border: "solid","borderColor": "#4A88CE" }} className="form form-control" placeholder="name here" ref={nameInput => this.nameInput = nameInput}/>
      </div>
      <div className="row">
      <span style={{color:"#4A88CE" , "fontWeight": "bold"}}>Surname : &nbsp;</span>
      <input required style={{border: "solid","borderColor": "#4A88CE" }} className="form form-control" placeholder="name here" ref={surnameInput => this.surnameInput = surnameInput}/>
      </div>
      <div className="row">
      <span style={{color:"#4A88CE" , "fontWeight": "bold"}}>Countries : &nbsp;</span>
      <select style={{border: "solid","borderColor": "#4A88CE" }} className="form form-control" ref={countriesInput => this.countriesInput = countriesInput}> 
        
       {
        this.state.comboBox.map(comboBox => {
          return(
           
            <option
           
           
          
            >{comboBox.name}</option>

            );
        })

       }
      
     </select>
      </div>
      <div className="row">
      <span style={{color:"#4A88CE" , "fontWeight": "bold"}}>Birthday : &nbsp;</span>
      <input required type="date"  style={{border: "solid","borderColor": "#4A88CE" }} className="form form-control" placeholder="mm/dd/yyy" ref={birthdayInput => this.birthdayInput = birthdayInput}/>
      </div>
      <br />
      <button style={{border: "solid","borderColor": "#4A88CE" , backgroundColor: "#EEEEEE" , color:"#4A88CE" , "fontWeight": "bold" , "float": "right" }} className="btn btn-default">Save</button>
      <br / >
      <br / >

      </form>
   
    );
  }
}

export default AddVisitor;
