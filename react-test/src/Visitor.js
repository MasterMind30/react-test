import React, { Component } from 'react';


class Visitor extends Component {
  constructor(props){
    super(props);

  } 



  render() {
  let {name, surname , countries , birthday } = this.props;
  let date = birthday.split("-");
  let year = date[0];
  let month = date[1];
  let day = date[2];
    return (
      <tbody>
         <tr>
          <th scope="row">{name}</th>
          <td>{countries}</td>
          <td>{day+"-"+month+"-"+year}</td>
        
        </tr>
      </tbody>
    );
  }
}

export default Visitor;
